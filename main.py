from flask import Flask
from flask_bootstrap import Bootstrap
from pymongo import MongoClient

app = Flask(__name__)
app.config.from_object('config')
db = MongoClient(app.config["DB_URL"])[app.config["DB_NAME"]]
Bootstrap(app)
import views