from flask.ext.wtf import Form
from wtforms import TextField, BooleanField, SubmitField, SelectField
from wtforms.ext.dateutil.fields import DateTimeField
from wtforms.validators import Required
from datetime import datetime, timedelta
from main import db
#sample form
class LoginForm(Form):
    openid = TextField('openid', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class TravelCreateForm(Form):
    now = datetime.today() + timedelta(hours=4)
    name = TextField('Traveller Name', validators = [Required()])
    time = DateTimeField('Date Time', validators = [Required()], default=now)
    airport = SelectField('Airport', validators = [Required()], choices=[("Bangalore", "Bangalore")])
    co_ordinate = TextField('co ordinates', validators = [Required()])
    submit = SubmitField("Add")

class AdminForm(Form):
    choices = []
    for obj in db.travel.distinct("time"):
        choices.append((obj.strftime("%Y-%m-%d %H:%M:%S"),obj.strftime("%Y-%m-%d %H:%M:%S")))
    airport = SelectField('Airport', validators = [Required()], choices=[("Bangalore", "Bangalore")])
    time = SelectField('Time', validators = [Required()], choices=choices)
    submit = SubmitField("Show rides")
