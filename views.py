from main import app, db
from forms import TravelCreateForm, AdminForm
from flask import render_template
import json
from datetime import datetime

@app.route('/',methods=('GET', 'POST'))
def index():
    form = TravelCreateForm()
    if form.validate_on_submit():
        time = form.time.data
        co_ordinate = form.co_ordinate.data.split("|")
        co_ordinate_tuple = (float(co_ordinate[0]), float(co_ordinate[1]))
        name = form.name.data
        airport = form.airport.data
        db.location.insert({
            "time" : time,
            "co_ordinate" : co_ordinate_tuple,
            "name" : name,
            "airport" : airport
        })
        message = name + " has been successfully added to the travel plan at " + time.strftime("%Y-%m-%d %H:%M")
        return render_template('index.html', form=form, message=message)
        #insert data and give a success message
    return render_template('index.html', form=form)

@app.route('/admin',methods=('GET', 'POST'))
def admin():
    form = AdminForm()
    if form.validate_on_submit():
        time = form.time.data
        airport = form.airport.data
        travels = []
        for travel in db.travel.find({"time" : datetime.strptime(time,"%Y-%m-%d %H:%M:%S")}):
            ind_group = []
            for passenger in travel["travel"]:
                ind_group.append({
                    "name" : passenger["name"],
                    "location" : passenger["co_ordinate"]
                })
            travels.append(ind_group)
        #get airport and time
        travels = json.dumps(travels)
        return render_template('admin.html', form=form, travels=travels)
        #insert data and give a success message
    return render_template('admin.html', form=form)