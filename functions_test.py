from functions import *
from datetime import datetime, timedelta
from config import DB_URL, DB_NAME
from pymongo import MongoClient
import unittest


db = MongoClient(DB_URL)["testdb"]
coordinates = [(1, 2), (2, 3), (5, 6), (7, 8), (5, 7), (5, 6.5), (1, 8)]
for x in range(0, 7):
    db.location.insert({
        "time": datetime.now(),
        "co_ordinate": coordinates[x],
        "name": "ENTRY" + str(x)
    })
currentTime = datetime.now()
formattedTime = currentTime + timedelta(hours=4) - timedelta(minutes=currentTime.minute, seconds=currentTime.second, microseconds=currentTime.microsecond)


class TestSequenceFunctions(unittest.TestCase):

    def test_nearest_neighbour(self):
        self.assertEquals(len(getNearest([5, 6], 3, db, formattedTime)), 3)
        self.assertEquals(len(getNearest([1, 2], 3, db, formattedTime)), 1)
        db.location.remove()
        db.travel.remove()

    def test_create_cluster(self):
        createClusters(db, formattedTime)
        self.assertEquals(db.travel.find().count(), 5)
        db.location.remove()
        db.travel.remove()