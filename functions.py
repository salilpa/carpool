from pygeocoder import Geocoder
import requests
from datetime import datetime, timedelta
from operator import itemgetter

radius = 0.015060 * 3


def getNearest(co_ordinate, count, db, time):
    dbCursor = db.location.find(
        {"co_ordinate": {"$within": {"$center": [list(co_ordinate), radius]}}, "time": {"$lt": time}}).limit(count)
    locations = []
    for location in dbCursor:
        locations.append(location)
    return locations


def getCoordinates(address):
    return list(Geocoder.geocode(address).coordinates)


def calculateDistance(origins, destinations):
    url = 'http://maps.googleapis.com/maps/api/distancematrix/json?sensor=false&mode=driving&avoid=tolls&'
    originURL = ''
    for location in origins:
        originURL = originURL + str(location[0]) + ',' + str(location[1]) + '|'

    destinationURL = ''
    for location in destinations:
        destinationURL = destinationURL + str(location[0]) + ',' + str(location[1]) + '|'

    url += 'origins=' + originURL[:-1] + '&destinations=' + destinationURL[:-1]

    headers = {
        'Accept-Language': 'en-US,en;q=0.8'
    }

    return requests.post(url, headers=headers).json()


# finding distances from origin to destinations
def findDistance(origins, destination, db):
    origin_coordinates = []

    for x in origins:
        origin_coordinates.append(x['co_ordinate'])

    googleResponse = calculateDistance(origin_coordinates, [destination[0]['co_ordinate']])

    for x in range(0, len(origins)):
        db.location.update({'_id': origins[x]['_id']},
                           {"$set": {'distance': googleResponse['rows'][x]['elements'][0]['distance']['value']}})
        origins[x]['distance'] = googleResponse['rows'][x]['elements'][0]['distance']['value']

    return origins


# input a list of locations(tuples) and form clusters
# output list of list of locations(tuples)
def createClusters(db, time):
    dbCursor = db.location.find({"time": {"$lt": time}})
    locations = []
    for location in dbCursor:
        locations.append(location)

    # destination hardcoded
    locations = findDistance(locations, [{'co_ordinate': getCoordinates("Bengaluru International Airport")}], db)

    while len(locations) > 0:
        travel = getNearest(locations[0]["co_ordinate"], 3, db, time)
        db.travel.insert({"travel": sorted(travel, key=itemgetter('distance'), reverse=True), "time": time})
        for x in travel:
            locations.remove(x)
        #creating a list of ids to be deleted
        ids = []
        for entry in travel:
            ids.append(entry["_id"])
        db.location.remove({"_id": {"$in": ids}})


def runJob(db):
    currentTime = datetime.now()
    # round to the next full hour
    createClusters(db, currentTime + timedelta(hours=4) -
                       timedelta(minutes=currentTime.minute, seconds=currentTime.second,
                                 microseconds=currentTime.microsecond))
